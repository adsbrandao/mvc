<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1-0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Materialize</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../src/css/materialize.min.css">
</head>
<body>
<nav>
    <div class="nav-wrapper green darken-4">
      <a href="index.php" class="brand-logo"><i class="fa fa-reply"></i></a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="adicionar.php">ADICIONAR CLIENTE</a></li>
        <li><a href="adicionarproduto.php">ADICIONAR PRODUTO</a></li>
        
      </ul>
    </div>
  </nav>

